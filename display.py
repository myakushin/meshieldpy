import ssd1306
from main import Status,State
from machine import I2C, Pin


class Display:
    def __init__(self):
        self.i2bus = I2C(1, scl=Pin(4), sda=Pin(5), freq=100000)
        self.disp = ssd1306.SSD1306_I2C(128, 64, self.i2bus)
        self.disp.text("inited", 0, 0)
        self.disp.show()

    def update(self, status):
        # assert isinstance(status, Status)
        wifi_status = status.last_wifi_status
        self.disp.fill(0)
        self.disp.text("W:" + wifi_status, 0, 0)
        if status.bt is None:
            self.disp.text("B:Loading...", 0, 10)
        else:
            self.disp.text("B:"+status.bt.status,0,10)


        f = 0
        for i in range(status.max_hit.get()):
            if i < status.now_hit.get():
                self.disp.fill_rect(f * 13, 20, 12, 20, 1)
                self.disp.text("B", f * 13 + 2, 27, 0)
            else:
                self.disp.rect(f * 13, 20, 12, 20, 1)
                self.disp.text("B", f * 13 + 2, 27, 1)

            f += 1
        for i in range(status.max_tech.get()):
            if i < status.now_tech.get():
                self.disp.fill_rect(f * 13, 20, 12, 20, 1)
                self.disp.text("A", f * 13 + 2, 27, 0)
            else:
                self.disp.rect(f * 13, 20, 12, 20, 1)
                self.disp.text("A", f * 13 + 2, 27, 1)

            f += 1
        for i in range(status.max_shield.get()):
            if i < status.now_shield.get():
                self.disp.fill_rect(f * 13, 20, 12, 20, 1)
                self.disp.text("S", f * 13 + 2, 27, 0)
            else:
                self.disp.rect(f * 13, 20, 12, 20, 1)
                self.disp.text("S", f * 13 + 2, 27, 1)

            f += 1
        if status.StateNow is not None:
            self.disp.text(status.StateNow.get_text(status), 0, 50, 1)
        self.disp.show()
