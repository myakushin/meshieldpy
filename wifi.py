import network
import os
import webrepl
import utils

wificfg = dict()
wlan = None


def load_conf_opt(filename, optname):
    wificfg[optname] = utils.load_conf(filename)


def load_wifi_conf():
    load_conf_opt("wifi_ssid", "ssid")
    load_conf_opt("wifi_password", "password")
    print(repr(wificfg))


def check_connect():
    global wlan
    if wlan is None:
        wlan = network.WLAN(network.STA_IF)
        wlan.active(True)
    if not wlan.isconnected():
        wlan.connect(wificfg['ssid'], wificfg['password'])
        webrepl.start(password='!QAZxsw2')


def get_status():
    if wlan is None:
        return "Not inited"
    if "ssid" not in wificfg:
        return "No config"
    if wificfg['ssid'] is None:
        return "No config"
    # assert isinstance(wlan, network.WLAN)
    if wlan.isconnected():
        return "C:" + wlan.ifconfig()[0]
    return "Not conn"
