import wifi
import display
import utime, time
import utils
import button
import strip
import machine
import ubluetooth
from micropython import const

RECHARGE_TIME = 5000
SCREEN_UPDATE = 300
NOW_HIT_UUID = ubluetooth.UUID("89d6717f-039c-4e6b-b68d-b2468c846057")
NOW_TECH_UUID = ubluetooth.UUID("8c611193-e464-4e12-8dfb-a1982ccf9071")
NOW_SHEILD_UUID = ubluetooth.UUID("15298eda-49b2-4fca-a3e2-f63d8cad817e")

FULL_HIT_UUID = ubluetooth.UUID("148bae7b-9d77-4a23-83cc-2bbf5b003b48")
FULL_TECH_UUID = ubluetooth.UUID("8d5de9ae-b4d4-47f0-a3f0-c8702914a1c7")
FULL_SHELD_UUID = ubluetooth.UUID("5657e13e-215c-4885-872c-ce32031a336c")

BT_STATE_UUID = ubluetooth.UUID("b65237db-d51f-48e8-86d9-a94f9355cbb6")
CHAR_NAME_UUID = ubluetooth.UUID("34bef2b0-204b-48fc-8a88-8d9fe3d12d1e")


# _UUID = ubluetooth.UUID("c63fcd32-a7a5-4f1d-9222-76bac6fec25f")


class Timer:
    def __init__(self):
        self.end = 0
        self.action = None

    def set(self, delay):
        self.end = utime.ticks_add(utime.ticks_ms(), delay)

    def check(self):
        if self.end == 0 or self.action is None:
            return
        if utime.ticks_diff(utime.ticks_ms(), self.end) > 0:
            self.action()
            self.end = 0


class State:
    def __init__(self):
        self.name = "State_Base"

    def enter(self, stat):
        pass

    def exit(self, stat):
        pass

    def update(self, stat):
        pass

    def get_text(self, stat):
        return self.name

    def hit(self, stat):
        pass


class IdleState(State):
    def __init__(self):
        super().__init__()
        self.name = "Idle"

    def enter(self, stat):
        stat.set_animation("idle")
        stat.hit_button.pressact = stat.hit
        stat.hit_button.holdact = lambda: stat.setstate(ChargingState())

    def update(self, stat):
        if stat.now_shield.get() == 0:
            stat.strip.clear()
        if stat.all_animation_complete():
            stat.set_animation("idle")

        if stat.max_shield.get() != stat.now_shield.get():
            stat.setstate(RechargeState())


class RechargeState(State):
    def __init__(self):
        super().__init__()
        self.name = "Recharge"
        self.timer = Timer()

    def enter(self, stat):
        # stat.set_animation("idle")
        self.timer.action = lambda: stat.set_animation("recharge")
        self.timer.set(RECHARGE_TIME)

    def get_text(self, stat):
        d = utime.ticks_diff(self.timer.end, utime.ticks_ms())
        if d > 0:
            return "Recharge in " + str(d)
        else:
            return "RECHARGE NOW!!"

    def update(self, stat):
        self.timer.check()
        if stat.now_shield.get() == 0:
            stat.strip.clear()
        if self.timer.end == 0 and stat.all_animation_complete():
            stat.now_shield.set(stat.max_shield.get())
            stat.setstate(IdleState())

    def hit(self, stat):
        self.timer.set(RECHARGE_TIME)


class ChargingState(State):
    def __init__(self):
        super().__init__()
        self.name = "CHAAARGE!!!!"
        self.timer = Timer()

    def enter(self, stat):
        stat.set_animation("charge")
        stat.hit_button.pressact = lambda: stat.setstate(IdleState())
        self.timer.action = lambda: stat.setstate(IdleState())
        self.timer.set(10 * 1000)

    def update(self, stat):
        self.timer.check()


class BleedingState(State):
    def __init__(self):
        super().__init__()
        self.name = "Bleed"
        self.timer = Timer()

    def get_text(self, stat):
        d = utime.ticks_diff(self.timer.end, utime.ticks_ms())
        if d > 0:
            return "Bleeding in " + str(d)
        else:
            return "BLEED NOW!!"

    def enter(self, stat):
        assert isinstance(stat, Status)
        stat.hit_button.pressact = None
        stat.hit_button.holdact = lambda: stat.setstate(WoundedState())
        stat.set_animation("bleed")
        self.timer.action = lambda: stat.setstate(DeadState())
        self.timer.set(10 * 1000)

    def update(self, stat):
        self.timer.check()


class WoundedState(State):
    def __init__(self):
        super().__init__()
        self.name = "Wounded"

    def enter(self, stat):
        stat.set_animation("wounded")
        stat.hit_button.pressact = None
        stat.hit_button.holdact = None


class DeadState(State):
    def __init__(self):
        super().__init__()
        self.name = "YOU DIED"

    def enter(self, stat):
        stat.hit_button.pressact = None
        stat.hit_button.holdact = None
        stat.heal_button.holdact = None
        stat.heal_button.pressact = None
        stat.set_animation("dead")


class Status:
    def __init__(self):
        self.bt = None
        self.last_screen_update = 0
        self.last_wifi_status = ""

        self.max_hit = BTChar(FULL_HIT_UUID, ubluetooth.FLAG_READ | ubluetooth.FLAG_WRITE, 2)
        self.max_tech = BTChar(FULL_TECH_UUID, ubluetooth.FLAG_READ | ubluetooth.FLAG_WRITE, 2)
        self.max_shield = BTChar(FULL_SHELD_UUID, ubluetooth.FLAG_READ | ubluetooth.FLAG_WRITE, 2)
        self.now_hit = BTChar(NOW_HIT_UUID, ubluetooth.FLAG_READ | ubluetooth.FLAG_WRITE | ubluetooth.FLAG_NOTIFY, 2)
        self.now_tech = BTChar(NOW_TECH_UUID, ubluetooth.FLAG_READ | ubluetooth.FLAG_WRITE | ubluetooth.FLAG_NOTIFY, 1)
        self.now_shield = BTChar(NOW_SHEILD_UUID, ubluetooth.FLAG_READ | ubluetooth.FLAG_WRITE | ubluetooth.FLAG_NOTIFY,
                                 1)
        self.bt_state_now = BTString(BT_STATE_UUID)

        self.StateNow = None

        self.disp = display.Display()

        self.heal_button = button.Button(13)
        self.hit_button = button.Button(15)
        self.hit_button.pressact = self.hit
        self.heal_button.pressact = lambda: print("HEAL")
        self.strip = strip.Strip(machine.Pin(25, machine.Pin.OUT), 11)
        self.setstate(IdleState())

    def setstate(self, newstate):
        assert isinstance(newstate, State)
        print("set state to " + newstate.name)
        if isinstance(self.StateNow, State):
            self.StateNow.exit(self)
        self.StateNow = newstate
        newstate.enter(self)
        self.bt_state_now.set(newstate.name)

    def set_animation(self, anim):
        at = strip.IdleAnimation
        print("Set animation to " + anim)
        if isinstance(anim, str):
            at = strip.animations[anim]
        elif issubclass(anim, strip.Animation):
            at = anim
        self.strip.set_animation(at(self.strip))

    def all_animation_complete(self):
        return self.strip.animation_complete()

    def get_params(self):
        ret = list()
        for f in self.__dict__.keys():
            if not isinstance(f, str):
                continue
            if f.startswith("max_") or f.startswith("now_"):
                ret.append(f)
        return ret

    def load_status(self):
        for param in self.get_params():
            ret = utils.load_conf(param)
            if isinstance(ret, int):
                self.__dict__[param] = ret

    def save_status(self):
        for param in self.get_params():
            f = open(param, "w")
            f.write(str(self.__dict__[param]))
            f.close()

    def hit(self):
        print("HIT")
        if self.now_shield.get() > 0:
            self.now_shield.dec()
        elif self.now_tech.get() > 0:
            self.now_tech.dec()
        elif self.now_hit.get() > 0:
            self.now_hit.dec()

        if self.now_hit.get() == 0:
            self.setstate(BleedingState())
        if self.now_shield.get() > 0:
            self.set_animation("hit")

        if isinstance(self.StateNow, State):
            self.StateNow.hit(self)

    def check(self):
        self.hit_button.check()
        self.heal_button.check()
        if utime.ticks_diff(utime.ticks_ms(), self.last_screen_update) > SCREEN_UPDATE:
            self.last_wifi_status = wifi.get_status()
            self.disp.update(self)
            self.last_screen_update = utime.ticks_ms()

        if isinstance(self.StateNow, State):
            self.StateNow.update(self)
        self.strip.update()


TARGET_TIME = 20


class BTChar:
    def __init__(self, uuid, flags=ubluetooth.FLAG_READ, def_value=0):
        self.ble = None
        self.uuid = uuid
        self.flags = flags
        self.desc = None
        self.condesc = -1
        self.value = int(def_value)

    def set(self, v):
        assert isinstance(v, int)
        self.value = v
        self.send()

    def get(self):
        return self.value

    def inc(self):
        self.value += 1
        self.send()
        return self.value

    def dec(self):
        self.value -= 1
        self.send()
        return self.value

    def send(self):
        print(str.format("Set {0} to {1}", self.uuid, self.value))
        if self.ble is None or self.desc is None:
            return
        self.ble.gatts_write(self.desc, self.value.to_bytes(1, 'big'))
        if self.condesc >= 0:
            self.ble.gatts_notify(self.condesc, self.desc, self.value.to_bytes(1, 'big'))

    def recv(self):
        if self.ble is None or self.desc is None:
            return
        b = self.ble.gatts_read(self.desc)
        print(str(b))
        self.value = b[0]
        print(str.format("Update {0} to {1}", self.uuid, self.value))

    def setup(self, ble, desc):
        self.ble = ble
        self.desc = desc
        self.send()


class BTString(BTChar):
    def __init__(self, uuid, flags= ubluetooth.FLAG_READ | ubluetooth.FLAG_WRITE | ubluetooth.FLAG_NOTIFY, def_value=""):
        super().__init__(uuid, flags, 0)
        self.value = def_value

    def set(self, v):
        assert isinstance(v,str)
        self.value = v
        self.send()

    def send(self):
        print(str.format("Set {0} to {1}", self.uuid, self.value))
        if self.ble is None or self.desc is None:
            return
        self.ble.gatts_write(self.desc, self.value.encode('ASCII'))
        if self.condesc >= 0:
            self.ble.gatts_notify(self.condesc, self.desc, self.value.encode('ASCII'))

    def recv(self):
        if self.ble is None or self.desc is None:
            return
        b = self.ble.gatts_read(self.desc)
        assert isinstance(b,bytes)
        print(b)
        self.value = b.decode('ASCII')

_IRQ_CENTRAL_CONNECT = const(1 << 0)
_IRQ_CENTRAL_DISCONNECT = const(1 << 1)
_IRQ_GATTS_WRITE = const(1 << 2)
_IRQ_GATTS_READ_REQUEST = const(1 << 3)


class BT:
    def __init__(self, stat):
        assert isinstance(stat, Status)
        self.status = "Not connected"
        SheildSerivceUUID = ubluetooth.UUID("c272318f-93bc-4e34-8b8a-81e2706f106a")

        self.chars = list()

        self.ble = ubluetooth.BLE()
        self.ble.active(True)
        self.ble.irq(self.bt_irq)
        self.chars.append(stat.now_hit)
        self.chars.append(stat.now_tech)
        self.chars.append(stat.now_shield)

        self.chars.append(stat.max_hit)
        self.chars.append(stat.max_tech)
        self.chars.append(stat.max_shield)
        self.chars.append(stat.bt_state_now)
        SheildService = (SheildSerivceUUID, self._get_btc_tuple())

        hnd = self.ble.gatts_register_services((SheildService,))
        print(hnd)
        for h, c in zip(hnd[0], self.chars):
            c.setup(self.ble, h)
        self.ble.gap_advertise(50000, adv_data=b'', resp_data=b'')

    def _get_btc_tuple(self):
        ret = list()
        for f in self.chars:
            ret.append((f.uuid, f.flags,))
        return ret

    def bt_irq(self, event, data):
        if event == _IRQ_CENTRAL_CONNECT:
            conn_handle, addr_type, addr = data
            self.status = "Conn"
            for f in self.chars:
                f.condesc = conn_handle
        elif event == _IRQ_CENTRAL_DISCONNECT:
            # A central has disconnected from this peripheral.
            conn_handle, addr_type, addr = data
            self.status = "disconnected"
            for f in self.chars:
                f.condesc = -1
        elif event == _IRQ_GATTS_WRITE:
            # A central has written to this characteristic or descriptor.
            conn_handle, attr_handle = data
            for c in self.chars:
                assert isinstance(c, BTChar)
                if c.desc == attr_handle:
                    c.recv()
                    break

        elif event == _IRQ_GATTS_READ_REQUEST:
            # A central has issued a read. Note: this is a hard IRQ.
            # Return None to deny the read.
            # Note: This event is not supported on ESP32.
            conn_handle, attr_handle = data
        else:
            print("Unkown event " + str(event))


if __name__ == "__main__":
    wifi.load_wifi_conf()
    wifi.check_connect()

    status = Status()
    status.load_status()
    wifi.check_connect()
    status.bt = BT(status)
    while True:
        start_time = utime.ticks_ms()

        status.check()
        end_time = utime.ticks_ms()
        diff = utime.ticks_diff(end_time, start_time)
        if diff < TARGET_TIME:
            utime.sleep_ms(TARGET_TIME - diff)

    #  status.save_status()
