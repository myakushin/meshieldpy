from neopixel import NeoPixel
from utime import ticks_ms, ticks_diff
import uos

animations = dict()


class Animation:
    def __init__(self, strip):
        self.strip = strip
        self.last_update = 0

    def start(self):
        pass

    def complete(self):
        return True

    def end(self):
        pass

    def update(self, time):
        pass


class HitAnimation(Animation):
    ANIM_TIME = 2000

    def __init__(self, strip):
        super().__init__(strip)
        self.start_time = 0

    def start(self):
        self.strip.clear()
        self.start_time = ticks_ms()

    def update(self, time):
        np = self.strip.np
        for i, r in zip(range(self.strip.len), uos.urandom(self.strip.len)):
            np[i] = (0, 0, int(r))
        np.write()

    def complete(self):
        return ticks_diff(ticks_ms(), self.start_time) > self.ANIM_TIME


class IdleAnimation(Animation):
    def __init__(self, strip):
        super().__init__(strip)

    def start(self):
        self.strip.np.fill((0, 0, 128))
        self.strip.np.write()

    def complete(self):
        return False


class BleedingAnimation(Animation):
    def __init__(self, strip):
        super().__init__(strip)
        self.state = False

    def start(self):
        self.strip.np.fill((0, 0, 128))
        self.strip.np.write()

    def complete(self):
        return False

    def update(self, time):
        if ticks_ms() - self.last_update < 500:
            return False
        self.last_update = ticks_ms()
        np = self.strip.np
        hot = self.state
        for i in range(self.strip.len):
            if hot:
                np[i] = (255, 255, 0)
            else:
                np[i] = (0, 0, 0)
            hot = not hot
        np.write()
        self.state = not self.state


class WoundedAnimation(Animation):
    def __init__(self, strip):
        super().__init__(strip)

    def start(self):
        np = self.strip.np
        hot = False
        for i in range(self.strip.len):
            if hot:
                np[i] = (255, 255, 0)
            else:
                np[i] = (0, 0, 0)
            hot = not hot
        np.write()

    def complete(self):
        return False


class DeadAnimation(Animation):
    def __init__(self, strip):
        super().__init__(strip)
        self.state = False

    def start(self):
        self.strip.np.fill((0, 0, 0))
        self.strip.np.write()

    def complete(self):
        return False

    def update(self, time):
        if ticks_ms() - self.last_update < 500:
            return False
        self.last_update = ticks_ms()
        np = self.strip.np
        hot = self.state
        for i in range(self.strip.len):
            if hot:
                np[i] = (0, 255, 0)
            else:
                np[i] = (0, 0, 0)
            hot = not hot
        np.write()
        self.state = not self.state


class RechargeAnimation(Animation):
    def __init__(self, strip):
        super().__init__(strip)
        self.lednow = 0
        self.leds = strip.len

    def start(self):
        self.lednow = 0
        self.strip.clear()

    def complete(self):
        return self.lednow >= self.leds

    def update(self, time):
        if ticks_ms() - self.last_update < 50:
            return False
        self.last_update = ticks_ms()
        np = self.strip.np
        np.fill((0, 0, 0))
        np[self.lednow] = (0, 0, 255)
        self.lednow += 1
        np.write()


class ChargeAnimation(Animation):
    def __init__(self, strip):
        super().__init__(strip)
        self.state = False

    def start(self):
        self.strip.np.fill((0, 0, 0))
        self.strip.np.write()

    def complete(self):
        return False

    def update(self, time):
        if ticks_ms() - self.last_update < 300:
            return False
        self.last_update = ticks_ms()
        np = self.strip.np
        hot = self.state
        for i in range(self.strip.len):
            if hot:
                np[i] = (255, 255, 255)
            else:
                np[i] = (0, 0, 0)
            hot = not hot
        np.write()
        self.state = not self.state


class StandbyAnimation(Animation):
    def __init__(self, strip):
        super().__init__(strip)


animations["recharge"] = RechargeAnimation
animations["idle"] = IdleAnimation
animations["hit"] = HitAnimation
animations["bleed"] = BleedingAnimation
animations["dead"] = DeadAnimation
animations["wounded"] = WoundedAnimation
animations["charge"] = ChargeAnimation

class Strip:
    def __init__(self, pin, length):
        self.len = length
        self.np = NeoPixel(pin, length)
        self.animation = None
        self.clear()

    def clear(self):
        self.np.fill((0, 0, 0))
        self.np.write()

    def set_animation(self, anim):
        assert isinstance(anim, Animation)
        if isinstance(self.animation, Animation):
            self.animation.end()
        self.animation = anim
        self.animation.start()

    def animation_complete(self):
        if isinstance(self.animation, Animation):
            return self.animation.complete()
        return True

    def update(self):
        if isinstance(self.animation, Animation):
            if not self.animation.complete():
                self.animation.update(0)
