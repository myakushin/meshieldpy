from machine import Pin
import utime


class Button:
    DEBOUNCE_TIME = 50
    HOLD_TIME = 5000

    def __init__(self, pin):
        self.pin = Pin(pin, Pin.IN, Pin.PULL_UP)
        self.presstime = 0
        self.holdtime = 0
        self.releasetime = 0
        self.pressact = None
        self.holdact = None
        self.pressed = False
        self.hold_played = False

    def check(self):
        now = utime.ticks_ms()
        prev = self.pressed
        if now - self.presstime < self.DEBOUNCE_TIME:
            return
        if now - self.releasetime < self.DEBOUNCE_TIME:
            return

        self.pressed = self.pin.value() == 0
        if self.pressed:
            if not prev:
                self.presstime = now
            else:
                self.holdtime = now - self.presstime

            if self.holdtime > self.HOLD_TIME:
                if not self.hold_played and self.holdact is not None:
                    self.holdact()
                self.hold_played = True
            else:
                self.hold_played = False
        else:
            if prev:
                self.releasetime = now
                if not self.hold_played and self.pressact is not None:
                    self.pressact()

