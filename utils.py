def load_conf(filename):
    try:
        optfile = open(filename, "r")
        l = optfile.readline()
        l = l.strip()
        if l.isdigit():
            return int(l)
        return l
    except OSError as e:
        print("Can`t open file " + filename + repr(e))
        return None
